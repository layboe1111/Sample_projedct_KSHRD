package edu.com.asus.sample.util;

import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * Created by ASUS on 6/20/2017.
 */

public class RequestUtil {
    public static RequestBody toRequestBody(String value){
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }
}
