package edu.com.asus.sample;

import android.support.v4.app.Fragment;

/**
 * Created by ASUS on 6/5/2017.
 */

public class Tab {
    private Fragment fragment ;
    private String title ;
    private int icons ;

    public Tab(Fragment fragment, String title, int icons) {
        this.fragment = fragment;
        this.title = title;
        this.icons = icons;
    }


    public Fragment getFragment() {
        return fragment;
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIcons() {
        return icons;
    }

    public void setIcons(int icons) {
        this.icons = icons;
    }

}
