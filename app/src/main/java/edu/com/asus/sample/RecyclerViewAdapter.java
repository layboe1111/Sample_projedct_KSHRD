package edu.com.asus.sample;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by ASUS on 5/29/2017.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.My_Viewholder> {


    private Context context ;
    private ArrayList<Player> playerslist ;

    public RecyclerViewAdapter(Context context, ArrayList<Player> playerslist) {
        this.context = context;
        this.playerslist = playerslist;
    }
    @Override
    public My_Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.itemlist , parent , false);
        return new My_Viewholder(v);
    }

    @Override
    public void onBindViewHolder(My_Viewholder holder, int position) {
        Player player = playerslist.get(position);
        holder.tvname.setText("Name: "+player.getName());
        holder.tvclub.setText("Club: "+player.getClub());
        holder.tvnumber.setText("Number: "+player.getNumber());
        Picasso.with(context).load(player.getImgurl()).into(holder.images);
    }

    @Override
    public int getItemCount() {
        return playerslist.size();
    }

    public static class My_Viewholder extends RecyclerView.ViewHolder{
        TextView tvname , tvclub , tvnumber  ;
        ImageView images ;
        public My_Viewholder(View itemView) {
            super(itemView);
            tvname = (TextView) itemView.findViewById(R.id.player_name);
            tvclub = (TextView) itemView.findViewById(R.id.playerClub);
            tvnumber = (TextView) itemView.findViewById(R.id.shirtnumber);
            images = (ImageView) itemView.findViewById(R.id.imglogo);
        }
    }
}
