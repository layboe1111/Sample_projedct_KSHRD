package edu.com.asus.sample;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;


public class HomeScreen extends AppCompatActivity {
    TabLayout tabLayout  ;
    ViewPager viewPager  ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);



        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        viewPager = (ViewPager) findViewById(R.id.viewPager);

        AdapterDynamicFragment fragadapter = new AdapterDynamicFragment(getSupportFragmentManager());
        fragadapter.addTab(new Tab(HomeFragment.newInstance("home") , "Home" ,R.drawable.ic_home_black_24dp ));
        fragadapter.addTab(new Tab(FavoriteFragment.newInstance("favorite") , "Favorite" , R.drawable.ic_favorite_black_24dp));

        viewPager.setAdapter(fragadapter);
        tabLayout.setupWithViewPager(viewPager);
        fragadapter.tablayoutIcon(tabLayout);

    }


}
