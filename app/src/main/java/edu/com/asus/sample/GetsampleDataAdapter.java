package edu.com.asus.sample;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import edu.com.asus.sample.entity.ArticalSample;

/**
 * Created by ASUS on 6/14/2017.
 */

public class GetsampleDataAdapter extends RecyclerView.Adapter<GetsampleDataAdapter.SampleDataHolder> {

    private Context context ;
    private ArrayList<ArticalSample.Data> articallist ;
    public GetsampleDataAdapter(Context context , ArrayList<ArticalSample.Data> articallist){
        this.context = context;
         this.articallist = articallist ;
    }
    @Override
    public SampleDataHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_artical_list , parent , false) ;
        return new SampleDataHolder(view);
    }

    @Override
    public void onBindViewHolder(SampleDataHolder holder, int position) {
        ArticalSample.Data data = articallist.get(position);
        holder.tvtitle.setText(data.getTitle());
        holder.tvdescription.setText(data.getDescription());
        holder.tvStatus.setText(data.getStatus());
    }

    @Override
    public int getItemCount() {
        return articallist.size();
    }

    public class SampleDataHolder extends RecyclerView.ViewHolder{
        TextView tvtitle , tvdescription , tvStatus  ;
        public SampleDataHolder(View itemView) {
            super(itemView);
            tvtitle = (TextView) itemView.findViewById(R.id.tvTitle);
            tvdescription = (TextView) itemView.findViewById(R.id.tvDes);
            tvStatus = (TextView) itemView.findViewById(R.id.tvstatus);
        }
    }
}
