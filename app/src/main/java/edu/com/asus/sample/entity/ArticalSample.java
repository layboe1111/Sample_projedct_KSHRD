package edu.com.asus.sample.entity;

import java.util.List;

/**
 * Created by ASUS on 6/14/2017.
 */

public class ArticalSample {

    @com.google.gson.annotations.SerializedName("CODE")
    private String code;
    @com.google.gson.annotations.SerializedName("MESSAGE")
    private String message;
    @com.google.gson.annotations.SerializedName("DATA")
    private List<Data> data;
    @com.google.gson.annotations.SerializedName("PAGINATION")
    private Pagination pagination;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    public static class Author {
        @com.google.gson.annotations.SerializedName("ID")
        private int id;
        @com.google.gson.annotations.SerializedName("NAME")
        private String name;
        @com.google.gson.annotations.SerializedName("EMAIL")
        private String email;
        @com.google.gson.annotations.SerializedName("GENDER")
        private String gender;
        @com.google.gson.annotations.SerializedName("TELEPHONE")
        private String telephone;
        @com.google.gson.annotations.SerializedName("STATUS")
        private String status;
        @com.google.gson.annotations.SerializedName("FACEBOOK_ID")
        private String facebookId;
        @com.google.gson.annotations.SerializedName("IMAGE_URL")
        private String imageUrl;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getTelephone() {
            return telephone;
        }

        public void setTelephone(String telephone) {
            this.telephone = telephone;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getFacebookId() {
            return facebookId;
        }

        public void setFacebookId(String facebookId) {
            this.facebookId = facebookId;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }
    }

    public static class Category {
        @com.google.gson.annotations.SerializedName("ID")
        private int id;
        @com.google.gson.annotations.SerializedName("NAME")
        private String name;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static class Data {
        @com.google.gson.annotations.SerializedName("ID")
        private int id;
        @com.google.gson.annotations.SerializedName("TITLE")
        private String title;
        @com.google.gson.annotations.SerializedName("DESCRIPTION")
        private String description;
        @com.google.gson.annotations.SerializedName("CREATED_DATE")
        private String createdDate;
        @com.google.gson.annotations.SerializedName("AUTHOR")
        private Author author;
        @com.google.gson.annotations.SerializedName("STATUS")
        private String status;
        @com.google.gson.annotations.SerializedName("CATEGORY")
        private Category category;
        @com.google.gson.annotations.SerializedName("IMAGE")
        private String image;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(String createdDate) {
            this.createdDate = createdDate;
        }

        public Author getAuthor() {
            return author;
        }

        public void setAuthor(Author author) {
            this.author = author;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Category getCategory() {
            return category;
        }

        public void setCategory(Category category) {
            this.category = category;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }

    public static class Pagination {
        @com.google.gson.annotations.SerializedName("PAGE")
        private int page;
        @com.google.gson.annotations.SerializedName("LIMIT")
        private int limit;
        @com.google.gson.annotations.SerializedName("TOTAL_COUNT")
        private int totalCount;
        @com.google.gson.annotations.SerializedName("TOTAL_PAGES")
        private int totalPages;

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getLimit() {
            return limit;
        }

        public void setLimit(int limit) {
            this.limit = limit;
        }

        public int getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(int totalCount) {
            this.totalCount = totalCount;
        }

        public int getTotalPages() {
            return totalPages;
        }

        public void setTotalPages(int totalPages) {
            this.totalPages = totalPages;
        }
    }
}
