package edu.com.asus.sample;

import java.util.ArrayList;

/**
 * Created by ASUS on 6/5/2017.
 */

public class Datasource {
    public static ArrayList<Player> players (){
        ArrayList<Player> datasource = new ArrayList<>();
        datasource.add(new Player(1 , "David Backham" ,"Manchester United" ,"7" , "http://3.bp.blogspot.com/-hUoP-kd4DR8/TkamEWAFEtI/AAAAAAAAALs/942r000Qn-8/s1600/Beckham+-+Man.+United+2.jpg" ));
        datasource.add(new Player(2, "Lionel Messi", "Bacelona" ,"10","http://images.performgroup.com/di/library/omnisport/13/32/lionel-messi-cropped_1x9992lcr150c1uss8yij7hkvz.jpg?t=1318778687&w=620&h=430"));
        datasource.add(new Player(3,"Micheal Owen","Real Mardrid","7","http://i.dailymail.co.uk/i/pix/2013/03/19/article-2295671-023C29E20000044D-686_634x438.jpg"));
        datasource.add(new Player(4 , "Zlatan Imbrahimovic" ,"AC Milan","10" ,"http://www2.pictures.zimbio.com/gi/Zlatan+Ibrahimovic+AC+Milan+v+Lecce+Serie+XNljRYaNhtXl.jpg"));
        datasource.add(new Player(5 ,"Cristiano Ronaldo", "Real Mardrid","7" ,"http://static.foot01.com/img/images/460x600/2016/Oct/30/real-madrid-cristiano-ronaldo-detient-un-record-hallucinant-iconsport_mar_291016_02_11,159506.jpg"));
        datasource.add(new Player(6 , "Robert Lewandowski","Bayer Munich","9","http://cdn.images.express.co.uk/img/dynamic/67/590x/Robert-Lewandowski-625037.jpg"));
        datasource.add(new Player(7,"Edan Hazard","Chelsea","10" ,"http://static.sportskeeda.com/wp-content/uploads/2016/02/eden-hazard-1455718526-800.jpg"));
        return datasource ;
    }
}
