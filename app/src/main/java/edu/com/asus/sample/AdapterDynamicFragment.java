package edu.com.asus.sample;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ASUS on 6/5/2017.
 */

public class AdapterDynamicFragment extends FragmentPagerAdapter {
    List<Tab> tabList ;
    public AdapterDynamicFragment(FragmentManager fm) {
        super(fm);
        tabList = new ArrayList<>();
    }

    @Override
    public Fragment getItem(int position) {

        return tabList.get(position).getFragment();

    }

    @Override
    public int getCount() {
        return tabList.size();
    }

    public void addTab(Tab tab){
        tabList.add(tab);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return null;
        //return tabList.get(position).getTitle();
    }
    public void tablayoutIcon(TabLayout tabLayout){
        for (int i = 0 ; i < tabList.size() ;i++ ){
            tabLayout.getTabAt(i).setIcon(tabList.get(i).getIcons());
        }
    }
}
