package edu.com.asus.sample;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import dmax.dialog.SpotsDialog;
import edu.com.asus.sample.entity.UserData;
import edu.com.asus.sample.retrofit.ServiceGenerator;
import edu.com.asus.sample.retrofit.ServiceUser;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginForm extends AppCompatActivity {
    private ServiceUser serviceUser ;
    private List<UserData.Data> dataList;
    EditText etuser , etpassowrd ;
    SpotsDialog dialog ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_form);
        TextView tvsignup = (TextView) findViewById(R.id.tvsignup);
        etuser = (EditText) findViewById(R.id.etuserlogin);
        etpassowrd = (EditText) findViewById(R.id.etPasswordsingup);
        dialog = new SpotsDialog(this, "Please Wait...!");
        serviceUser = ServiceGenerator.createService(ServiceUser.class);

        tvsignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginForm.this , SignUp.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.btnlogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadRetrofit();
            }
        });
        findViewById(R.id.btnfakeapi).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginForm.this , GetSampleData.class));
            }
        });
    }
    public void loadRetrofit(){
        Call<UserData> call = serviceUser.getUserNameandPassowrd();
        dialog.show();
        call.enqueue(new Callback<UserData>() {
            @Override
            public void onResponse(Call<UserData> call, Response<UserData> response) {

                UserData data =  response.body();
                dataList = new ArrayList<UserData.Data>();
                dataList = data.getData();
                Log.e("ooooo" , dataList.toString());
                UserData.Data userAccount = new UserData.Data();
                    if (etuser.equals(userAccount.getEmail())){
                        startActivity(new Intent(LoginForm.this , HomeScreen.class));
                    }
                    else {
                        Toast.makeText(LoginForm.this, "Fail", Toast.LENGTH_SHORT).show();
                    }

                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<UserData> call, Throwable t) {
                t.printStackTrace();
                dialog.dismiss();
            }
        });
    }

}
