package edu.com.asus.sample.retrofit;

import edu.com.asus.sample.entity.ArticalSample;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by ASUS on 6/14/2017.
 */

public interface ArticalService {
    @GET("v1/api/articles")
    Call<ArticalSample> findallArtical() ;
}
