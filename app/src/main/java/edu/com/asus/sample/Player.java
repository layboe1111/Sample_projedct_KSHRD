package edu.com.asus.sample;

/**
 * Created by ASUS on 5/29/2017.
 */

public class Player {
    private int id ;
    private String name ;
    private String club ;
    private String number ;
    private String imgurl ;

    public Player(int id, String name, String club, String number, String imgurl) {

        this.id = id;
        this.name = name;
        this.club = club;
        this.number = number;
        this.imgurl = imgurl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClub() {
        return club;
    }

    public void setClub(String club) {
        this.club = club;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }


}
