package edu.com.asus.sample;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import java.io.File;

import dmax.dialog.SpotsDialog;
import edu.com.asus.sample.retrofit.ServiceGenerator;
import edu.com.asus.sample.retrofit.ServiceUser;
import edu.com.asus.sample.util.RequestUtil;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUp extends AppCompatActivity {
    EditText etEmail , etUser , etPassword , etTelphone ;
    Button btnBroswe , btnUpload;
    public String imagePath;
    ServiceUser serviceUser ;
    private int READ_EXTERNAL_STORAGE_CODE =1;
    private int OPEN_GALLERY =1 ;
    private SpotsDialog dialog ;
    private ImageView ivThumnail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        serviceUser = ServiceGenerator.createService(ServiceUser.class);
        dialog = new SpotsDialog(SignUp.this, "Processing Data");
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPasswordsingup);
        etUser = (EditText) findViewById(R.id.etUser);
        etTelphone = (EditText) findViewById(R.id.etTel);
        btnBroswe = (Button) findViewById(R.id.btnBroswe);
        btnUpload = (Button) findViewById(R.id.btnUpload);


        btnBroswe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    int checkpermission = ContextCompat.checkSelfPermission(SignUp.this, Manifest.permission.READ_EXTERNAL_STORAGE);
                    String[] permission = {Manifest.permission.READ_EXTERNAL_STORAGE};
                    if (checkpermission == PackageManager.PERMISSION_DENIED) {
                        requestPermissions(permission, READ_EXTERNAL_STORAGE_CODE);
                    } else {
                        showGallery();
                    }
                } else {
                    showGallery();
                }
            }
        });

    btnUpload.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            RequestBody email = RequestUtil.toRequestBody(etEmail.getText().toString());
            RequestBody name = RequestUtil.toRequestBody(etUser.getText().toString());
            RequestBody password = RequestUtil.toRequestBody(etPassword.getText().toString());
            RequestBody gender = RequestUtil.toRequestBody("M");
            RequestBody tel = RequestUtil.toRequestBody(etTelphone.getText().toString());
            RequestBody facebookid = RequestUtil.toRequestBody("1");
            File file = new File(imagePath);
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            MultipartBody.Part photo = MultipartBody.Part.createFormData("PHOTO", file.getName(), requestBody);
            Call<JsonObject> uploadPerson = serviceUser.createUser(email , name , password , gender , tel , facebookid , photo);
            dialog.show();
            uploadPerson.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.isSuccessful()){
                        Toast.makeText(SignUp.this, "Success", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    t.printStackTrace();
                    dialog.dismiss();
                }
            });


        }
    });
    }
    void showGallery(){
        Intent galleryIntent = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, OPEN_GALLERY);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == READ_EXTERNAL_STORAGE_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showGallery();
            } else {
                Toast.makeText(this, "Please Grant Permisson", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ivThumnail= (ImageView) findViewById(R.id.ivThumnail);
        Uri selectImage = data.getData();
        String[] pathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(selectImage , pathColumn , null, null ,null);
        if (cursor.moveToFirst()){
            int columnIndex = cursor.getColumnIndex(pathColumn[0]);
            imagePath =cursor.getString(columnIndex);
            Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
            ivThumnail.setImageBitmap(bitmap);
        }
    }
}
