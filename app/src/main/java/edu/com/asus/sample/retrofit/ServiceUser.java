package edu.com.asus.sample.retrofit;

import com.google.gson.JsonObject;

import edu.com.asus.sample.entity.UserData;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by ASUS on 6/20/2017.
 */

public interface ServiceUser {
    @Multipart
    @POST("/v1/api/users")
    Call<JsonObject> createUser (
            @Part("EMAIL")RequestBody email ,
            @Part("NAME") RequestBody name ,
            @Part("PASSWORD") RequestBody passoword ,
            @Part("GENDER") RequestBody gender ,
            @Part("TELEPHONE") RequestBody telephone ,
            @Part("FACEBOOK_ID") RequestBody facebook_id ,
            @Part MultipartBody.Part photo
            );

    @GET("/v1/api/users")
    Call<UserData> getUserNameandPassowrd();

}
