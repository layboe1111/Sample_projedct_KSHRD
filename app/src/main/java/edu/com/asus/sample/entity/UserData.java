package edu.com.asus.sample.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ASUS on 6/21/2017.
 */

public class UserData {

    @SerializedName("CODE")
    private String code;
    @SerializedName("MESSAGE")
    private String message;
    @SerializedName("DATA")
    private List<Data> data;
    @SerializedName("PAGINATION")
    private Pagination pagination;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    public static class Data {
        @SerializedName("ID")
        private int id;
        @SerializedName("NAME")
        private String name;
        @SerializedName("EMAIL")
        private String email;
        @SerializedName("GENDER")
        private String gender;
        @SerializedName("TELEPHONE")
        private String telephone;
        @SerializedName("STATUS")
        private String status;
        @SerializedName("FACEBOOK_ID")
        private String facebookId;
        @SerializedName("IMAGE_URL")
        private String imageUrl;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getTelephone() {
            return telephone;
        }

        public void setTelephone(String telephone) {
            this.telephone = telephone;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getFacebookId() {
            return facebookId;
        }

        public void setFacebookId(String facebookId) {
            this.facebookId = facebookId;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", email='" + email + '\'' +
                    ", gender='" + gender + '\'' +
                    ", telephone='" + telephone + '\'' +
                    ", status='" + status + '\'' +
                    ", facebookId='" + facebookId + '\'' +
                    ", imageUrl='" + imageUrl + '\'' +
                    '}';
        }
    }

    public static class Pagination {
        @SerializedName("PAGE")
        private int page;
        @SerializedName("LIMIT")
        private int limit;
        @SerializedName("TOTAL_COUNT")
        private int totalCount;
        @SerializedName("TOTAL_PAGES")
        private int totalPages;

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getLimit() {
            return limit;
        }

        public void setLimit(int limit) {
            this.limit = limit;
        }

        public int getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(int totalCount) {
            this.totalCount = totalCount;
        }

        public int getTotalPages() {
            return totalPages;
        }

        public void setTotalPages(int totalPages) {
            this.totalPages = totalPages;
        }
    }
}
